var gulp = require('gulp');
var connect = require('gulp-connect');
var dest = require('gulp-dest');

var cors = function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
};

gulp.task('server:test', function () {
  connect.server({
    root: 'project',
    livereload: true,
    port: 8080,
    middleware: function () {
      return [cors];
    }
  });
  gulp.src(['*.html',])
  .pipe(gulp.dest(paths.tmp));
});