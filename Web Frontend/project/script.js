
function resetDisplay() {
    document.getElementById("page1").style.display = "none";
    document.getElementById("page2").style.display = "none";
    document.getElementById("page3").style.display = "none";
    document.getElementById("page1Menu").style.color = "white";
    document.getElementById("page2Menu").style.color = "white";
    document.getElementById("page3Menu").style.color = "white";
}

function htmlEncode (value){
  return $('<div/>').text(value).html();
}

function generateQrCode() {
    $("#qrCode").attr("src", "https://chart.googleapis.com/chart?cht=qr&chl=" + htmlEncode($("#inputPage2").val()) + "&chs=160x160&chld=L|0");
}

function changeUrl(url) {
    var stateObject = {};
    var title = "Wow Title";
    history.pushState(stateObject,title, url);
}

function selectPage(view, objId) {
    resetDisplay();
    document.getElementById(objId).style.display = "inline";
    document.getElementById(objId + "Menu").style.color = "red";
    changeUrl(view);

    if (objId == "page3"){
        var stuffValue = document.getElementById("inputPage2").value;
        var iframe = document.getElementById("iframeContainer");
        iframe.src = stuffValue;
        console.log('iframe.contentWindow =', iframe.contentWindow);
        var window = iframe.contentWindow;
        var doc = iframe.contentWindow.document;
        console.log(doc);
    }
}

function changeInput() {
    var value = document.getElementById("inputPage2").value;
    console.log(value);
    var code = makeXHR(value);
    var codeStr = code.toString();
    console.log(codeStr);
    if (codeStr.charAt(0) == "2") {
        document.getElementById("inputPage2").style.backgroundColor = "green";
    } else {
        document.getElementById("inputPage2").style.backgroundColor = "red";
    }
    generateQrCode();
}


function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    console.log(theUrl);
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}


function makeXHR(url) {

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function (data) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                  console.log("Response is received");
            }
        } else {
            //callback(null);
        }
    }

    var url = 'http://www.w3schools.com/html/default.asp';
    xhr.open('GET', url, true);
    xhr.send();

    console.log(xhr);
    console.log(xhr.status);
    return xhr.status;

}