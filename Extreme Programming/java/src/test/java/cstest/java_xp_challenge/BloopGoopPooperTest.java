package cstest.java_xp_challenge;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class BloopGoopPooperTest {

  private static final List<Integer> INT_LIST = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  private static final List<Integer> UNMATCHING_INT_LIST = Arrays.asList(4, 8, 14, 16, 22, 26, 28);
  private static final List<Integer> RANDOM_INT_LIST = Arrays.asList(15, 30, 839, 853, 999, 1000, 1665);

  private static final String MULTIPLE_OF_3 = "bloop";
  private static final String MULTIPLE_OF_5 = "poop";
  private static final String PRIME = "goop";

  private static final String MULTIPLE_OF_3_AND_5 = String.format("%s-%s", MULTIPLE_OF_3, MULTIPLE_OF_5);
  private static final String PRIME_AND_MULTIPLE_OF_3 = String.format("%s-%s", MULTIPLE_OF_3, PRIME);
  private static final String PRIME_AND_MULTIPLE_OF_5 = String.format("%s-%s", MULTIPLE_OF_5, PRIME);

  private BloopGoopPooper bloopGoopPooper;

  @Before
  public void setup() {
    bloopGoopPooper = new BloopGoopPooper();
  }

  @Test
  public void givenAListOfIntegers_whenBloopPoopize_thenReturnTheHashMap() {
    HashMap<Integer, String> map = bloopGoopPooper.bloopPoopize(INT_LIST);

    HashMap<Integer, String> expectedMap = new HashMap<Integer, String>();
    expectedMap.put(2, PRIME);
    expectedMap.put(3, PRIME_AND_MULTIPLE_OF_3);
    expectedMap.put(5, PRIME_AND_MULTIPLE_OF_5);
    expectedMap.put(6, MULTIPLE_OF_3);
    expectedMap.put(7, PRIME);
    expectedMap.put(9, MULTIPLE_OF_3);
    expectedMap.put(10, MULTIPLE_OF_5);

    assertThat(map).isEqualToComparingFieldByField(expectedMap);
  }

  @Test
  public void givenIntegersThatDoesNotMatch_whenBloopPoopize_thenTheMapIsEmpty() {
    HashMap<Integer, String> map = bloopGoopPooper.bloopPoopize(UNMATCHING_INT_LIST);

    assertThat(map).isEmpty();
  }

  @Test
  public void givenRandomIntegers_whenBloopPoopize_thenReturnTheHashMap() {
    HashMap<Integer, String> map = bloopGoopPooper.bloopPoopize(RANDOM_INT_LIST);

    HashMap<Integer, String> expectedMap = new HashMap<Integer, String>();
    expectedMap.put(15, MULTIPLE_OF_3_AND_5);
    expectedMap.put(30, MULTIPLE_OF_3_AND_5);
    expectedMap.put(839, PRIME);
    expectedMap.put(853, PRIME);
    expectedMap.put(999, MULTIPLE_OF_3);
    expectedMap.put(1000, MULTIPLE_OF_5);
    expectedMap.put(1665, MULTIPLE_OF_3_AND_5);

    assertThat(map).isEqualToComparingFieldByField(expectedMap);
  }
}
