# Extreme programming - C

## Simulation OO en C - Le décorateur à des fins de logging.

### Pré-requis
* Avoir un compilateur de C99 ou plus sur votre machine (gcc/clang/etc.)

### But

Bienvenue au challenge d'extreme programming en C, une épreuve qui mettera en pratique vos habiletés avec un langage de bas niveau. Le but de ce challenge est d'implémenter l'équivalent d'un décorateur (patron de conception) pour créer un simple logger en C, un langage qui ne se veut pas *objet*. 

#### Apriori

Nous avons deux types de véhicules: une Harley Davidson et une Honda Civic donc le fonctionnement est implémenté dans les modules `HarleyDavidson.c / .h` et `HondaCivic.c / .h`.

On réalise une conception dite objet de ces deux concepts au moyen de pointeurs de `struct` contenant les attributs de l'équivalent de la classe.

Pour arriver à gérer la mémoire comme le ferait un langage objet, on utilise une technique de *reference counting* (voir module `ReferencedObject.c / .h`).

Par convention, les méthodes publiques sont déclarées dans le `.h` et les privées sont déclarées `static` dans le `.c`.

On simule une interface commune de véhicule à l'aide d'une structure de pointeurs de fonctions qui définissent la signature des méthodes à implémenter qui sera contenue dans les attributs de la classe l'implémentant (voir module `VehicleInterface.h`).

#### Ce que vous devez réaliser

Vous devez compléter les modules `VehicleLogger.c / .h` aux endroits indiqués dans le code par les commentaires afin d'implémenter un logger qui fonctionne pour tout véhicule implémentant `VehicleInterface`.
Un fichier `main.c` est fourni pour tester le tout. Le logger est instancié au moyen de la méthode `VehicleLogger_new` pour un véhicule particulier. Lorsqu'on appelle la méthode `startLogging`, tout appel subséquent aux méthodes publiques du véhicule *logged* doit s'inscrire de la sorte dans le fichier `log.txt` tout en conservant ses comportements initiaux:

e.g. log.txt
```
------------ NEW EXECUTION ------------

Engine started
Speed set to 75 km/h
Flashers turned on
Steered of 90°
Amount of fuel remaining in tank: 1L
Engine stopped


------------ END OF EXECUTION ------------



------------ NEW EXECUTION ------------

Engine started
Speed set to 40 km/h
Flashers turned off
Steered of -10°
Amount of fuel remaining in tank: 41L
Engine stopped


------------ END OF EXECUTION ------------
```

Comparez cet exemple avec ce qui se passe dans le `main.c`, vous pourrez déduire ce que doit écrire dans le logger chacune des méthodes publiques d'un véhicule.

De plus, lorsqu'on appelle la méthode `stopLogging`, tout appel subséquent aux méthodes publiques du véhicule *logged* ne doit plus s'inscrire dans le fichier `log.txt` et conserver ses comportements initiaux.

#### Modalités de remise

Vous devez remettre le code avec les fichiers `VehicleLogger.c / .h` modifiés (uniquement eux sont autorisés à être modifiés). De plus, vous devez fournir un `Makefile` qui permet la compilation du projet respectant les contraintes suivantes:

- Il doit compiler le code lorsque la commande `make` est envoyée et retourner un executable nommé `main`
- Il doit compiler avec les *debug informations* activées
- Il doit créer un fichier `.o` pour tous les fichiers `.c` donnés et les mettre dans un dossier `/obj`
- Il doit permettre avec la commande `make clean` de supprimer tous les fichiers `.o` du dossier `/obj` et l'exécutable produit précédement (`main`).

Sortez vos skills de pointeurs de fonction et bonne chance! 

