# Extreme programming - TypeScript

## Une question de timing

### Pré-requis
* Avoir nodejs et npm installés sur votre machine

## Instruction
* `npm install`
* `npm test` pour rouler les tests.
* Modifier seulement l'intérieur de la fonction `fetchMessageByUserId` dans `/src/main.ts` pour faire passer les tests. Ne pas modifier sa signature.
* Vous avez le droit d'ajouter des fonctions.

## But
Imaginez qu'on a deux micro services, le premier dans lequel on peut aller chercher un nom d'utilisateur à partir de son id et un second dans lequel on peut aller chercher une donnée par nom d'utilisateur. Ces services sont asynchrones. Utilisez donc des Promises pour pouvoir aller chercher le nom d'utilisateur puis, avec celui-ci, aller chercher le message. Retourner une promise qui resolve pour le message lorsqu'on lui passe l'id de Bob. `resolve(message)`;

* Vous devez appeler les fonctions `fetchUserNameById` et `fetchMessageByUserName` et les résoudre de façon asynchrone pour retourner le message.
* Pas le droit d'écrire simplement `resolve('Hi!');` dans la fonction interne de la promise! Oui, ça fait passer le test, mais c'est pas le but de l'exercice!
